import React, { Component } from 'react';
import { connect } from 'react-redux';
import Scrollbars from '../../../components/utility/customScrollBar';
import actions from '../../../redux/instagramWidget/actions';
import {
  InstagramWidget,
  UserInformation,
  UserAction,
  Media,
  MediaItem,
} from './style';
const { getUserData } = actions;

class InstagramFeed extends Component {
  componentWillMount() {
    this.props.getUserData();
  }

  render() {
    const { userData, stretched } = this.props;
   // const profilePicUrl = userData ? userData.info.profile_picture : null;
   const profilePicUrl = userData ;

    const showMedia = (item, index) => {
      const imageUrl = item.images.thumbnail.url;
      return (
        <MediaItem key={index}>
          <img alt="#" src={imageUrl} />
        </MediaItem>
      );
    };
    return (
      <InstagramWidget stretched={stretched}>
        <UserInformation>
          <h3></h3>
           <p></p>
        </UserInformation>

        <UserAction>
          <div className="profilePicture">
            <img alt="#" src={profilePicUrl} />
          </div>

          <div className="mediaCounter">
            <div className="mediaCounterItem">
              <h5></h5>
              <p>Images</p>
            </div>

            <div className="mediaCounterItem">
              <h5></h5>
              <p>Followers</p>
            </div>

            <div className="mediaCounterItem">
              <h5></h5>
              <p>Following</p>
            </div>
          </div>
        </UserAction>

        <Scrollbars style={{ height: 150 }}>
          <Media>

          </Media>
        </Scrollbars>
      </InstagramWidget>
    );
  }
}

function mapStateToProps(state) {
  return {
    userData: state.InstagramWidget.toJS().userData,
    loading: state.InstagramWidget.toJS().loading,
  };
}
export default connect(mapStateToProps, { getUserData })(InstagramFeed);
