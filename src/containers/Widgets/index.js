import React, { Component } from 'react';
import LayoutWrapper from '../../components/utility/layoutWrapper';
import {
  Row,
  OneFourthColumn,
  OneThirdColumn,
  TwoThirdColumn,
  FullColumn,
  HalfColumn,
} from '../../components/utility/rowColumn';
import InstagramFeed from './InstagramFeed';
import Contacts from '../Contact/contactBox';
import Statistics from './Statistics';
import Transaction from './Transactions';
import SalesProgress from './SaleProgress';
import SalesStats from './Sales';
import SaleChart from './SaleCharts';
import TableWidget from './TableWidget';
import CircularWidget from './CircularWidgets';
import { data, data2, data3 } from './Transactions/config';

class Widget extends Component {
  render() {
    return (
      <LayoutWrapper>
        <Row>
          <FullColumn>
            <TableWidget title="Group Data" />
          </FullColumn>
        </Row>

        <Row>
          <FullColumn>
            <TableWidget title="School Data" />
          </FullColumn>
        </Row>

        <Row>
          <FullColumn>
            <TableWidget title="Employees Data" />
          </FullColumn>
        </Row>

        <Row>
          <FullColumn>
            <TableWidget title="Students Data" />
          </FullColumn>
        </Row>

        <Row>
          <FullColumn>
            <TableWidget title="Device Data" />
          </FullColumn>
        </Row>

      </LayoutWrapper>
    );
  }
}

export default Widget;
