import { getDefaultPath } from '../../helpers/urlSync';

const options = [
  {
    label: 'sidebar.group',
    key: 'group',
    leftIcon: 'select_all',
    hideBreadCrumb: true,
  },
  {
    label: 'sidebar.invoice',
    key: 'invoice',
    leftIcon: 'select_all',
    hideBreadCrumb: true,
  },
  {
    label: 'sidebar.contact',
    key: 'contact',
    leftIcon: 'contacts',
    hideBreadCrumb: true,
  },
  {
    label: 'sidebar.calendar',
    key: 'calendar',
    leftIcon: 'event',
    hideBreadCrumb: true,
  },
  {
    label: 'sidebar.forms',
    key: 'forms',
    leftIcon: 'receipt',
    children: [
      {
        label: 'sidebar.reduxForms',
        key: 'redux-forms',
      }
    ],
  },
  {
    label: 'sidebar.notes',
    key: 'notes',
    leftIcon: 'note',
  },

  {
    label: 'sidebar.pages',
    leftIcon: 'public',
    key: 'pages',
    children: [
      {
        label: 'sidebar.404',
        key: '404',
        withoutDashboard: true,
      },
      {
        label: 'sidebar.505',
        key: '505',
        withoutDashboard: true,
      },
      {
        label: 'sidebar.signIn',
        key: 'signin',
        withoutDashboard: true,
      },
      {
        label: 'sidebar.signUp',
        key: 'signup',
        withoutDashboard: true,
      },
      {
        label: 'sidebar.forgotPassword',
        key: 'forgot-password',
        withoutDashboard: true,
      },
      {
        label: 'sidebar.resetPassword',
        key: 'reset-password',
        withoutDashboard: true,
      },
    ],
  },
  {
    label: 'sidebar.blankPage',
    key: 'blank-page',
  },
];
const getBreadcrumbOption = () => {
  const preKeys = getDefaultPath();
  let parent, activeChildren;
  options.forEach(option => {
    if (preKeys[option.key]) {
      parent = option;
      (option.children || []).forEach(child => {
        if (preKeys[child.key]) {
          activeChildren = child;
        }
      });
    }
  });
  return { parent, activeChildren };
};
export default options;
export { getBreadcrumbOption };
